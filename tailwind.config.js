module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#03dac6',
        secondary: '#002f33',
      }
    },
    fontFamily: {
      'sans': ['Roboto', 'ui-sans-serif', 'sistem-ui', 'sans-serif'],
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
